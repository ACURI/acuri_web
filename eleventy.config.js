const Image = require("@11ty/eleventy-img");

function wrapFigure(output, caption) {
    return `
    <figure>
      ${output}
      <figcaption>${caption}</figcaption>
    <figure>
  `;
}

function generateHTML(metadata, imageAttributes, filesrc) {
    // use the lower resolution width, height and url for the img
    let lowsrc = metadata.jpeg[0];

    const id =  filesrc.replace(/.*\/(.*)\.[a-z0-9]*/, "$1")
    return `<picture id="photo-${id}">
    ${Object.values(metadata)
      .map((imageFormat) => {
          return `  <source type="${
            imageFormat[0].sourceType
          }" srcset="${imageFormat
            .map((entry) => entry.srcset)
            .join(", ")}" sizes="${imageAttributes.sizes}">`;
      })
      .join("\n")}
      <img
        src="${lowsrc.url}"
        width="${lowsrc.width}"
        height="${lowsrc.height}"
        alt="${imageAttributes.alt}"
        loading="lazy"
        decoding="async">
    </picture>`;
}

let counter=0
async function imageShortcode(src, alt, sizes = "", caption = "") {
    const started = new Date().getTime()
    // create images and return the files metadata
    const metadata = await Image(src, {
        widths: [320, 640, 1280, 2880],
        formats: ["avif", "webp", "jpeg"],
        urlPath: "/images/",
        outputDir: "./_site/images/",    });

    const imageAttributes = {
        alt,
        sizes,
    };
    if(new Date().getTime()-started>200) console.log("Processed image " + src)
    const pictureOutput = generateHTML(metadata, imageAttributes, src);

    return caption ? wrapFigure(pictureOutput) : pictureOutput;
}


module.exports = function (eleventyConfig) {
    eleventyConfig.setServerOptions({showAllHosts: true})
    eleventyConfig.addPassthroughCopy({"src/static/":"/"})
    eleventyConfig.addPassthroughCopy({"src/img/":"/"})
    eleventyConfig.addShortcode("convertFromEpub", require("./src/_11ty/convertFromEpub"));

    const now = new Date().getTime()+"";
    eleventyConfig.addGlobalData("builtAt", now)




    eleventyConfig.addAsyncShortcode("image", imageShortcode);

    return {
        dir: {
            input: 'src',
            output: '_site',
            includes: '_includes',
            layouts: '_layouts'
        }
    };
};
