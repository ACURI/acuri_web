const yauzl = require("yauzl");
const fs = require('node:fs')
const path = require('node:path')

async function convertFromEpub(filename, left, top) {
    let expectedMainFile = filename.replace(/.*\/([^\/]*)/, "$1");
    expectedMainFile = expectedMainFile.replace(/([^.]*)\.[a-zA-Z]+/, "$1")
    expectedMainFile = expectedMainFile.replaceAll(" ", "-")
    expectedMainFile = expectedMainFile + "-1.xhtml"
    let content = "", css = ""
    //console.log("here's all we'd have ", this)
    const page = this.page
    filename = filename.startsWith("/") ? filename : path.join(path.dirname(page.inputPath), filename);

    // check if the page can be fetched from cache

    const cacheFile = path.join(".cache", "convertFromEpub", (page.inputPath + "__" + filename).replaceAll("/", "_"))
    fs.mkdirSync(".cache/convertFromEpub", {recursive:true})
    let stat1 = null, stat2 = null;
    try { stat1 = fs.statSync(cacheFile)} catch (e) {}
    try { stat2 = fs.statSync(page.inputPath)} catch (e) {}
    if(stat1 && stat1.isFile() && stat1.mtime >= stat2.mtime) {
        //console.log("Cache " + new Date(stat1.mtime) + " " + " page " + new Date(stat2.mtime))
        console.log("our cache file is current")
        return fs.readFileSync(cacheFile)
    }




    await new Promise((res, rej) => yauzl.open(filename, {lazyEntries: true}, function (err, zipfile) {
        return new Promise((resolve, reject) => {
            if (err) throw err;
            zipfile.readEntry();
            zipfile.on("entry", function (entry) {
                //console.log("Entry ", entry.fileName, expectedMainFile)
                if (/\/$/.test(entry.fileName)) {
                    // Directory file names end with '/'.
                    // Note that entries for directories themselves are optional.
                    // An entry's fileName implicitly requires its parent directories to exist.
                    zipfile.readEntry();
                } else {
                    // file entry
                    zipfile.openReadStream(entry, function (err, readStream) {
                        if (err) throw err;
                        let inContent = (entry.fileName.endsWith(expectedMainFile))
                        let inCSS = (entry.fileName.endsWith("idGeneratedStyles.css"))
                        let tobeExported = (entry.fileName.endsWith(".png") || entry.fileName.endsWith(".jpg") || entry.fileName.endsWith(".jpeg") || entry.fileName.endsWith(".webp") || entry.fileName.endsWith(".svg"))
                        let dir = entry.fileName.replace(/OEBPS\/(.*\/)[^/]*/, "$1")
                        let exportStream = null;
                        if(tobeExported) {
                            let fileout = page.outputPath.replace(/(.*\/)[^\/]*/, "$1" ) + entry.fileName.replace(/OEBPS\/(.*)/, "$1")
                            console.log("Would export: " + entry.fileName + " to dir " + fileout)
                            fs.mkdirSync(fileout + dir, {recursive:true})
                            exportStream = fs.createWriteStream(fileout, {flush: true});
                        }
                        //if(inContent) console.log("File ", entry.fileName, expectedMainFile)
                        readStream.on("end", function () {
                            zipfile.readEntry();
                        });
                        readStream.on('data', (chunk) => {
                            if(tobeExported) exportStream.write(chunk)
                            if(inContent) content = content + chunk.toString("utf-8")
                            if(inCSS) css = css + chunk.toString('utf-8')
                        });
                        readStream.on('end', () => {
                            if(tobeExported) {
                                //fs.fdatasync(exportStream.fd, (err) => {if (err!==null) {reject(err)}});
                                exportStream.close()
                            }
                        });
                    });
                }
            });
            zipfile.on("end", () => {
                zipfile.close();
                res(content)
            })
        })
    }));
    let re = /<body ([^>]*)>/, match = re.exec(content)
    content = content.substring(match.index + match[0].length)
    const atts = match[1]
    match = /<\/body>.*/.exec(content)
    content = content.substring(0,match.index)
    css = makeCssScale(css,atts, left, top)
    content = `<style>
        ${css}
        </style>
        <div class="convertedFromId">
    ${content}
    </div>`

    //console.log("content ", content)
    fs.writeFileSync(cacheFile, content)
    return content

}

function makeCssScale(css, atts, left, top) {
    left = Number.parseFloat(left); top = Number.parseFloat(top)
    let width = Number.parseInt(atts.replace(/.*width: ?([0-9]*)px.*/, "$1"))
    height = Number.parseInt(atts.replace(/.*height: ?([0-9]*)px.*/, "$1"))
    let zoomfactor = 100.0/width;
    const transformX = (x) => { return (Number.parseFloat(x)*zoomfactor)}
    const transformY = (y) => { return (Number.parseFloat(y)*zoomfactor)}

    css = `.convertedFromId { height: ${top+height*zoomfactor}vw } 
        ${css}`,
    css = css.replace(/body, div,(.*) \{/, ".convertedFromId div,$1 {");
    css = css.replace(/translate\(([0-9\.]*)px,([0-9\.]*)px\)/g, (match, dx, dy) => {
        return `translate(${transformX(dx+left)}vw, ${transformY(dy+top)}vw)`
    })
    css = css.replace(/(left|top|height|width): ?([0-9\.]*)px/g, (match, prop, val) => {
        if('left'===prop) return `${prop}: ${transformX(val)+ left}vw`
        if('width'===prop) return `${prop}: ${transformX(val) }vw`
        if('top'===prop) return `${prop}: ${transformY(val)+top}vw`
        if('height'===prop) return `${prop}: ${transformY(val)}vw`

    })

    css = css.replace(/position: ?absolute/g, "position: fixed" )
    return `/* made scalable from a viewport of (${width}px,${height}px) */\n` + css
}

module.exports = convertFromEpub