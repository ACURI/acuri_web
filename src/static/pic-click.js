

let clearPicClick = null
let picClickFrag = null

function handlePicClick(evt) {
  // trigger a hash change following a clicked photo
  const i = evt.target && evt.target.parentElement && evt.target.parentElement.id
  if(i) window.location.href = "#" + i.replace(/photo-(.*)/, "$1")
}


function handleHashChange(evt) {
  // analyze the hash to display the photo
  if(!evt.newURL) return
  const n = evt.newURL.replace(/.*#(.*)/, "$1")
  if(n===evt.newURL || n==="" || n===null) return
  console.log("Hash change " + n)
  let done = changePicClickFrag(n)
  if(done && evt.preventDefault) evt.preventDefault()
}

function changePicClickFrag(n) {
  // process the frag-id change, find the photo, request to display
  if(!picClickFrag || picClickFrag !== n) {
    let pic = document.getElementById("photo-" + n)
    if(clearPicClick) clearPicClick()
    if(!pic) return false
    window.setTimeout(()=>focusPic(pic), 5)
    return true
  }
  return false
}

function focusPic(pic) {
  // display the photo at picture element pic
  const mask = document.createElement("div")
  mask.setAttribute("class", "mask")
  const focusPicDiv = document.createElement("div")
  if(!pic) return
  focusPicDiv.setAttribute("class", "focus-pic")
  let altContent = pic.querySelector("img")
  if(altContent) altContent = altContent.getAttribute("alt")
  focusPicDiv.innerHTML =
    pic.outerHTML.replace(/id="[^"]*"/, "") +
    (altContent ? `<p class="focus-pic-caption">${altContent}</p>` : "")
  document.body.appendChild(focusPicDiv)
  document.body.appendChild(mask)
  clearPicClick = () => {
    mask.remove(); focusPicDiv.remove(); clearPicClick = null
    history.pushState({}, "", window.location.href.replace(/(.*)#.*/,"$1"))
  }
  mask.addEventListener("click", clearPicClick)
  focusPicDiv.addEventListener("click", clearPicClick)
}

// register the listeners
for(let pic of document.querySelectorAll(".photo picture")) {
  pic.addEventListener("click",handlePicClick)
}
document.addEventListener("keydown", (evt) => {
  if(evt.key === "Escape" && clearPicClick )  clearPicClick()
})
window.addEventListener("hashchange", handleHashChange)
handleHashChange({newURL: document.location.href})